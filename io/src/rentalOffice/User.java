package rentalOffice;

public class User {
	String username = "noname";
	String password = "";
	String type = "C"; //C - client, S - stuff
	
	/**
	 * Function inits user from given string
	 * @param str <type>,<username>,<password>
	 * @throws Exception 
	 */
	public void setFromString(String str) throws Exception {
		String[] argv = str.split(",");
		Integer used = 0;

		if(used < argv.length) {
			type = argv[used];
			if(!type.equals("C") && !type.equals("S"))
				throw new Exception("Given user type is incorrect. Pass value C (for client) or S (for stuff)");
			used ++;
		}
		
		if(used < argv.length) {
			username = argv[used];
			used ++;
		}
		
		if(used < argv.length) {
			password = argv[used];
			used ++;
		}
	}
	
	public Boolean verifyPassword(String password) {
		if(this.password.equals(password))
			return true;
		return false;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getType() {
		return type;
	}
	
	@Override
    public String toString() { 
        return (type.equals("C") ? "(Client)" : "(Stuff )") + " " + username;
    } 
}
