package rentalOffice;

import java.util.ArrayList;
import java.util.Iterator;

public class UserManager {

	ArrayList<User> users = new ArrayList<User>();
	
	public User logIn(String username, String password) {
		Iterator<User> it = users.iterator();
		
		while(it.hasNext()) {
			User u = it.next();
			if(u.getUsername().equals(username)) {
				if(u.verifyPassword(password) == true) {
					return u;
				}
			}
		}
		
		return null;
	}
	
	public void createUser(String userString) throws Exception {
		User u = new User();
		u.setFromString(userString);
		users.add(u);
	}
	
	public ArrayList<User> getUsers() {
		return users;
	}
	
}
