package rentalOffice;

import java.util.ArrayList;
import java.util.Iterator;

public class RentalManager {
	ArrayList<Rent> rents = new ArrayList<Rent> ();
	
	public void createRent(User user, String rentString) throws Exception {
		Rent r = new Rent(user);
		
		String[] argv = rentString.split(",");
		Integer used = 0;
		
		//valid data
		if(argv.length > 0) {	
			Integer id = Integer.parseInt(argv[used]);
			if(checkIfRentIdAvailable(id) == false)
				throw new Exception("id (" + id+ ") is beeing used");	
		}
		
		r.setManager(this);
		r.setFromString(rentString);
		rents.add(r);
	}
	
	public Rent findRentById(Integer id) throws Exception {
		Iterator<Rent> it = rents.iterator();
		
		while(it.hasNext()) {
			Rent i = it.next();
			if(i.getId() == id)
				return i;
		}
		
		throw new Exception("There is no rent identyfied by id: " + id.toString());
	}
	
	public void deleteRent(Rent rent) {
		rents.remove(rent);
	}
	
	public Rent findRentByIdAndUser(Integer id, User user) throws Exception {
		Iterator<Rent> it = rents.iterator();
		
		while(it.hasNext()) {
			Rent i = it.next();
			if(i.getId() == id && i.getUser() == user)
				return i;
		}
		
		throw new Exception("There is no rent identyfied by id: " + id.toString() + " owned by " + user.toString());
	}
	
	public ArrayList<Rent> getAll() {
		return rents;
	}
	
	public Boolean checkIfRentIdAvailable(Integer id) {
		Iterator<Rent> it = rents.iterator();
		
		while(it.hasNext())
			if(it.next().getId() == id)
				return false;
		
		
		return true;
	}
	
	public ArrayList<Rent> findUsersRent(User user) {
		Iterator<Rent> it = rents.iterator();
		ArrayList<Rent> rentsList = new ArrayList<Rent>();
		while(it.hasNext()) {
			Rent r = it.next();
			if(r.getUser() == user) {
				rentsList.add(r);
			}
		}
		
		return rentsList;
	}
}
