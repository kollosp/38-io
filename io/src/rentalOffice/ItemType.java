package rentalOffice;

import java.util.ArrayList;

public class ItemType {
	ArrayList<Item> items = new ArrayList<Item>();
	
	Integer id;
	String model;
	Float pricePerHour;
	ItemTypeManager manager;
	
	public void setFromString(String itemTypeString) {
		
		String[] argv = itemTypeString.split(",");
		Integer used = 0;
		
		if(used < argv.length) {
			setId(Integer.parseInt(argv[used]));
			used ++;
		}
		
		if(used < argv.length) {
			setModel(argv[used]);
			used ++;
		}
		
		if(used < argv.length) {
			setPricePerHour(Float.parseFloat(argv[used]));
			used ++;
		}
	}
	
	public void setManager(ItemTypeManager manager) {
		this.manager = manager;
	}
	
	/**
	 * Function creates new item
	 * @param itemString <id>,<available>,<manufacturer>,<description>,<quality>
	 * @throws Exception 
	 */
	public void createItem(String itemString) throws Exception {
		
		Item item = new Item();
		String[] argv = itemString.split(",");
		Integer used = 0;
		
		//valid data
		if(argv.length > 0) {	
			Integer id = Integer.parseInt(argv[used]);
			if(manager.checkIfItemIdAvailable(id) == false)
				throw new Exception("id (" + id+ ") is beeing used");	
		}
		
		item.setFromString(itemString);
			
		item.setType(this);
		
		items.add(item);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public void setPricePerHour(Float pricePerHour) {
		this.pricePerHour = pricePerHour;
	}
	
	public Float getPricePerHour() {
		return pricePerHour;
	}
	
	public ArrayList<Item> getItemList() {
		return items;
	}
	
	@Override
    public String toString() { 
        return id + "|" + model + " | " + pricePerHour;
    } 
}
